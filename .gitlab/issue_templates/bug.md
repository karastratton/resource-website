> Use this template to tell us about something not working properly, or a 'bug'.
> Do not use this template for requesting something new. For that, use the
> "Initial Feature Request" template.
>
> Following these templates will make it much easier for us to help you.
>
> After you have filled out this template, remove all the lines starting with `>`.

# Steps to reproduce
> Here, describe what you were doing when the bug occurred. Include as much
> detail as possible, so that we can cause the same bug to happen. If needed,
> include screenshots.


# Expected Outcome
> Here, describe what you though should happen

# Actual Outcome
> Here, describe what actually happened. If needed, include screenshots.