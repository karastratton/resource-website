import argparse
import csv
from django.core.management.base import BaseCommand, CommandError

from django.contrib.auth.models import Group

from user_management.models import CustomUser


class Command(BaseCommand):
    help = (
        "Imports users from a csv file in the format \n"
        "first name,last name,banner id,email,card uid,card number,staff,superuser,password,group 1,group 2,...,group n\n"
        "There can be any number of groups. If a user with the email already exists, it will be updated\n"
        "Only the email is required. A user will only be created if all info is there.\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)

        default_group, _ = Group.objects.get_or_create(name='All Users')

        for i, row in enumerate(reader):

            print("Importing {: 6}".format(i), end='')

            first_name = row[0]
            last_name = row[1]
            banner_id = row[2]
            email = row[3]
            staff = row[6]
            superuser = row[7]
            password = row[8]
            groups = row[9:]
            username = email.split('@')[0]

            user = CustomUser.objects.filter(email=email).first()

            if user is None:
                if len(first_name) > 0 and len(last_name) > 0 and len(banner_id) > 0 and len(email) > 0 and len(email) > 0:
                    user = CustomUser(
                        first_name=first_name,
                        last_name=last_name,
                        banner_id=banner_id,
                        email=email,
                        username=username
                    )
                else:
                    self.stderr.write("Warning: Not creating user because there is not enough information:\n{}".format(row))
                    continue

            if len(first_name) > 0:
                user.first_name = first_name
            if len(last_name) > 0:
                user.last_name = last_name
            if len(username) > 0:
                user.username = username
            if len(banner_id) > 0:
                user.banner_id = int(banner_id)
            if len(staff) > 0:
                user.is_staff = bool(int(staff))
            if len(superuser) > 0:
                user.is_superuser = bool(int(superuser))
            if len(password) > 0:
                user.set_password(password)

            user.save()

            user.groups.add(default_group)

            for group_name in groups:
                group, created = Group.objects.get_or_create(name=group_name)
                user.groups.add(group)

            user.save()

            print('\r', end='')
