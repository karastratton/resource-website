from django.http import HttpResponse
from django.shortcuts import render

from django.contrib.auth.decorators import login_required, permission_required

from .models import Page, PageStatus

@login_required(login_url='/login/google-oauth2/')
def index(request):
    pages = Page.objects.filter(status="LISTED")

    context = {
        'pages': pages,
    }

    return render(request, 'wiki/index.html', context)

@login_required(login_url='/login/google-oauth2/')
def page(request, slug):
    page = Page.objects.get(slug=slug)

    if not page.status == "DRAFT":
        context = {
            'page': page,
        }
        return render(request, 'wiki/page.html', context)
    else:
        return index(request)
