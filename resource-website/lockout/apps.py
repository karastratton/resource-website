from django.apps import AppConfig


class LockoutConfig(AppConfig):
    name = 'lockout'
