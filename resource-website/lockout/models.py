from django.db import models
from django.conf import settings
from django.utils import timezone
from trainings.models import Training, TrainingGroup, UserTraining


def machine_image_upload_to(machine, filename):
    return 'uploads/lockout/machine_images/{}/{}'.format(machine.id, filename)


class Machine(models.Model):
    name = models.CharField(max_length=64, default="")
    training_groups = models.ManyToManyField(TrainingGroup, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to=machine_image_upload_to)

    def __str__(self):
        return "{}".format(self.name)

    def can_operate(self, user):
        # This checks to see if a given user can operate a given machine
        for tg in self.training_groups.all():
            if tg.check_user(user):
                return True

    class Meta:
        permissions = (
            ("use_lockout", "Can use lockout"),
        )


class MachineWorkEntry(models.Model):
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE)
    description = models.TextField(default="")
    technician_notes = models.TextField(default="", null=True, blank=True)
    created = models.DateTimeField(default=timezone.now)
    resolved = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = "machine work entry"
        verbose_name_plural = "machine work entries"


class MachineUseEntry(models.Model):
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE)
    start = models.DateTimeField(default=timezone.now)
    end = models.DateTimeField(null=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "machine use entry"
        verbose_name_plural = "machine use entries"
