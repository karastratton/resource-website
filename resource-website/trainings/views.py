import os
import io
import csv

import logging

import pytz
from django.shortcuts import render
from django.shortcuts import redirect

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone

from django.http import HttpResponse
from django.http import FileResponse
from django.http import Http404

from social_django.utils import load_strategy
from django.contrib.auth.decorators import login_required, user_passes_test, permission_required
from django.core.exceptions import PermissionDenied

from .models import *

from .render import Render
from django.views.generic import View

from tests.models import Test


@login_required(login_url='/login/google-oauth2/')
def view_index(request):
    user_trainings = UserTraining.objects.filter(trainee=request.user)
    available_trainings = Training.objects.all()

    context = {
        'user_trainings': user_trainings,
        'available_trainings': available_trainings
    }
    return render(request, 'trainings/index.html', context)


@login_required(login_url='/login/google-oauth2/')
def view_certificate(request, user_training_id):
    usertraining = UserTraining.objects.get(pk=user_training_id)
    trainee = usertraining.trainee

    certificate = usertraining.training.certificate
    name = usertraining.training.certificate.name
    template = usertraining.training.certificate.html_template
    preimages = CertificateImage.objects.filter(certificate=certificate)
    images = []
    for image in preimages:
        images.append(image.image.path)

    params = {
        'usertraining': usertraining,
        'trainee': trainee,
        'images': images,
        'request': request
    }
    return Render.render(certificate.html_template.path, params)


@login_required(login_url='/login/google-oauth2/')
def view_training_details(request, training_id):
    training = Training.objects.get(pk=training_id)
    tests = Test.objects.filter(trainings=training)

    context = {
        'training': training,
        'tests': tests,
    }
    return render(request, 'trainings/details.html', context)


@login_required(login_url='/login/google-oauth2/')
def view_training_group_details(request):
    user_trainings = UserTraining.objects.filter(trainee=request.user)
    available_trainings = Training.objects.all()
    all_training_groups = TrainingGroup.objects.all()
    user_training_groups = []

    for group in TrainingGroup.objects.all():
        if group.check_user(request.user):
            user_training_groups.append(group)



    context = {
        'user_trainings': user_trainings,
        'available_trainings': available_trainings,
        'all_training_groups': all_training_groups,
        'user_training_groups': user_training_groups,
    }
    return render(request, 'trainings/tgcheck.html', context)
