from django.contrib import admin
from .models import Training, TrainingGroup, UserTraining, Certificate, CertificateImage


class TrainingAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_per_page = 25


class TrainingGroupAdmin(admin.ModelAdmin):
    list_display = ('group_name',)
    list_per_page = 25


class UserTrainingAdmin(admin.ModelAdmin):
    list_display = ('training', 'trainee', 'expiration', 'earned')
    list_per_page = 25


class CertificateImageAdmin(admin.ModelAdmin):
    list_per_page = 25


class CertificateImageInline(admin.StackedInline):
    model = CertificateImage
    extra = 0


class CertificateAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_per_page = 25
    inlines = [CertificateImageInline,]


# Register views
admin.site.register(Training, TrainingAdmin)
admin.site.register(TrainingGroup, TrainingGroupAdmin)
admin.site.register(UserTraining, UserTrainingAdmin)
admin.site.register(Certificate, CertificateAdmin)
#admin.site.register(CertificateImage, CertificateImageAdmin)
