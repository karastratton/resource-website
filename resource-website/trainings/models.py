from django.db import models
from django.conf import settings
from django.utils import timezone
from datetime import date

import logging

log = logging.getLogger(__name__)


def certificate_upload_to(certificate, filename):
    return 'uploads/trainings/certificates/{}/{}'.format(certificate.name, filename)


def certificate_image_upload_to(certificateimage, filename):
    return 'uploads/trainings/certificates/{}/images/{}'.format(certificateimage.certificate.name, filename)


class Certificate(models.Model):
    name = models.CharField(max_length=64, default="")
    html_template = models.FileField(upload_to=certificate_upload_to)

    def __str__(self):
        return "{} ({})".format(self.name, self.id)

    @property
    def template_url(self):
        return self.html_template.path


class CertificateImage(models.Model):
    image = models.ImageField(upload_to=certificate_image_upload_to)
    certificate = models.ForeignKey(Certificate, on_delete=models.CASCADE)


class Training(models.Model):
    title = models.CharField(max_length=64, default="")
    description = models.TextField(default="")
    certificate = models.ForeignKey(Certificate, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.title

    class Meta:
        permissions = (
            ('view_user_trainings', 'Can view user trainings'),
        )


class UserTraining(models.Model):
    training = models.ForeignKey(Training, on_delete=models.CASCADE)
    trainee = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    expiration = models.DateField()
    earned = models.DateField(default=timezone.now)

    def __str__(self):
        return "{}  - {}".format(self.training, self.trainee)

    @property
    def expired(self):
        return self.expiration < date.today()


class TrainingGroup(models.Model):
    group_name = models.CharField(max_length=64, default="")
    trainings = models.ManyToManyField(Training)

    def __str__(self):
        return self.group_name

    def check_user(self, user):
        log.info("Checking user {}".format(user))
        for training in self.trainings.all():
            ut_count = UserTraining.objects.filter(trainee=user, training=training).count() > 0
            if ut_count:
                ut = UserTraining.objects.get(trainee=user, training=training)
                if ut.expired:
                    return False
                else:
                    return True
            else:
                return False

