# Generated by Django 3.0.2 on 2020-05-28 01:04

from django.db import migrations, models
import trainings.models


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0004_auto_20200527_1207'),
    ]

    operations = [
        migrations.CreateModel(
            name='CertificateImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to=trainings.models.certificate_upload_to)),
            ],
        ),
        migrations.CreateModel(
            name='Certificate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=64)),
                ('html_template', models.FileField(upload_to=trainings.models.certificate_upload_to)),
                ('images', models.ManyToManyField(to='trainings.CertificateImage')),
            ],
        ),
    ]
