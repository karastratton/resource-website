import random

from bootstrap_datepicker_plus.widgets import DateTimePickerInput
from django import forms
from django.forms import inlineformset_factory

from .models import WorkEntry, WorkLocation

class WorkStartForm(forms.ModelForm):
    class Meta:
        model = WorkEntry
        fields = ['work_location', 'work_start']
        widgets = {
            'work_start': DateTimePickerInput(),
        }

class WorkEndForm(forms.ModelForm):
    class Meta:
        model = WorkEntry
        fields = ['work_end', 'work_description']
        widgets = {
            'work_end': DateTimePickerInput(),
            'work_description': forms.TextInput(),
        }
    #work_description = forms.CharField()
    #work_end = forms.DateTimeField()

class WorkEditForm(forms.ModelForm):
    class Meta:
        model = WorkEntry
        fields = ['work_location', 'work_start', 'work_end', 'work_description']
        widgets = {
            'work_start': DateTimePickerInput(),
            'work_end': DateTimePickerInput(),
            'work_description': forms.TextInput(),
        }

class CreateWorkLocationForm(forms.ModelForm):
    class Meta:
        model = WorkLocation
        fields = ['name']