from django.contrib import admin

# Register your models here.

from .models import WorkEntry, WorkLocation

#admin.site.register(WorkEntry)
admin.site.register(WorkLocation)

class WorkEntryAdmin(admin.ModelAdmin):
    list_display = ('worker', 'work_location', 'work_start', 'work_end', 'work_description')
    list_per_page = 25

admin.site.register(WorkEntry, WorkEntryAdmin)
