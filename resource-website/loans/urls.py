from django.urls import path

from . import views

urlpatterns = [
    path('loans', views.index, name='loans'),
]
