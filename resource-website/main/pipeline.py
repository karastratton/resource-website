# Various functions for the auth pipeline

from social_core.pipeline.partial import partial

from django.shortcuts import redirect, reverse

from django.contrib.auth.models import Group, User, Permission

from urllib.parse import urlencode


@partial
def request_user_info(strategy, details, user=None, is_new=False, *args, **kwargs):
    if user is not None:
        banner_id = user.banner_id
    else:
        banner_id = None

    if is_new or banner_id is None:
        # These must be specified in SOCIAL_AUTH_FIELDS_STORED_IN_SESSION in settings.py
        banner_id = strategy.session.get('banner_id') or banner_id

        if banner_id is None:
            redirect_base_url = reverse('request_user_info')

            if banner_id is not None:
                redirect_query = urlencode({'banner_id': banner_id})
            elif banner_id is None:
                redirect_query = ""

            redirect_url = '{}?{}'.format(redirect_base_url, redirect_query)
            return redirect(redirect_url)

        details['banner_id'] = banner_id

        return {
            'banner_id': banner_id,
        }


def add_default_group(backend, user, response, *args, **kwargs):
    default_group, created = Group.objects.get_or_create(name='All Users')

    if created:
        can_view_test = Permission.objects.get(name="Can take test", content_type__app_label="tests", content_type__model="test")
        default_group.permissions.add(can_view_test)

    user.groups.add(default_group)
