
// Submits a form, then reloads the page. Avoids the form submit POST request going in the browser history.
// Use in a form element like this:
//<form onsubmit="submit_form(event)">
function submit_form(e) {
    e.preventDefault();
    let form = e.target;
    let form_data = new FormData(form);
    let url = form.baseURI;
    fetch(url, { method: 'POST', body: form_data, }).then(function() {location.reload()});
}
