# Generated by Django 3.1 on 2020-08-23 16:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_partlocation_primary_location'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='part_name_template',
            field=models.CharField(blank=True, max_length=512, null=True),
        ),
    ]
