# Generated by Django 3.0.2 on 2020-08-23 04:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0007_partlocation_primary_location'),
    ]

    operations = [
        migrations.AddField(
            model_name='property',
            name='unit',
            field=models.CharField(blank=True, max_length=16),
        ),
    ]
