from django import forms
from django.forms import inlineformset_factory, modelformset_factory, DateInput
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, ButtonHolder, Submit, HTML, Button

from pages.crispy import InputGroupLayout, DynamicFormsetHelper

from .models import *


class HomepageSearchForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.layout = InputGroupLayout(
            'search',
            append=Div(ButtonHolder(
                HTML("<button type='submit' class='btn btn-primary'><i class='fas fa-search'></i></button>"),
            )),
            css_class="mb-3"
        )

    search = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control-lg'}))


class SupplierImportForm(forms.Form):
    category = forms.ModelChoiceField(Category.objects.all())
    part_numbers = forms.CharField(widget=forms.Textarea)


class PartLocationForm(forms.Form):
    part_numbers = forms.CharField(widget=forms.Textarea)