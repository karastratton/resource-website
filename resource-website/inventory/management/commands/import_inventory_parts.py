import argparse
import csv
from django.core.management.base import BaseCommand

from inventory.models import Supplier, Part, Category, Manufacturer, PartSupplier, Location, PartLocation, Property, PartProperty


class Command(BaseCommand):
    help = (
        "Imports inventory parts from a csv file in the format \n"
        "Category,InternalPartNo,Name Override,Desc,Mfg,Mfg Part No,Datasheet,Supplier=SupplierPartNo=SupplierUrl=SupplierPrice|..,Prop1=Value1|Prop2=Value2|..,Loc1=Qty=Barcode=Primary|..\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')

            category_path= row[0]
            internal_partno = row[1]
            name_override = row[2]
            desc = row[3]
            mfg = row[4]
            mfg_partno = row[5]
            datasheet = row[6]
            suppliers = row[7]
            properties = row[8]
            locations = row[9]

            part = Part.objects.filter(id=int(internal_partno)).first()

            if part is None:
                part = Part(id=internal_partno)

            if len(category_path) > 0:
                categories = category_path.split('/')

                category = None
                for category_name in categories:
                    current_category = Category.objects.filter(name=category_name, parent=category).first()
                    category = current_category

                part.category = category

            part.save()

            if len(name_override) > 0:
                part.name_override = name_override

            if len(desc) > 0:
                part.description = desc

            if len(mfg) > 0:
                manufacturer = Manufacturer.objects.get(name=mfg)
                part.manufacturer = manufacturer

            if len(mfg_partno) > 0:
                part.manufacturer_part_number = mfg_partno

            if len(datasheet) > 0:
                part.datasheet = datasheet

            for supplier in suppliers.split('|'):
                supplier = supplier.split('=')
                supplier_name = supplier[0]
                supplier_partno = supplier[1]
                supplier_url = supplier[2]
                supplier_price = supplier[3]

                supplier = Supplier.objects.get(name=supplier_name)

                part_supplier = part.partsupplier_set.filter(supplier=supplier).first()

                if part_supplier is None:
                    part_supplier = PartSupplier(supplier=supplier, part=part)

                if len(supplier_partno) > 0:
                    part_supplier.supplier_part_number = supplier_partno

                if len(supplier_url) > 0:
                    part_supplier.supplier_part_url = supplier_url

                if len(supplier_price) > 0:
                    part_supplier.price = supplier_price

                part_supplier.save()

            for property in properties.split('|'):
                property = property.split('=')
                name = property[0]
                value = property[1]
                properties_q = part.category.properties_q()
                property = Property.objects.filter(properties_q).get(name=name)
                part_property = PartProperty.objects.filter(property=property, part=part).first()
                if part_property is None:
                    part_property = PartProperty(property=property, part=part, value=value)
                    part_property.save()
                else:
                    part_property.value = value
                    part_property.save()

            for location in locations.split('|'):
                location = location.split('=')
                path = location[0]
                qty = location[1]
                barcode = location[2]
                primary = location[3]

                location = None
                for location_name in path.split('/'):
                    current_location = Location.objects.filter(name=location_name, parent=location).first()
                    location = current_location

                part_location = PartLocation.objects.filter(part=part, location=location).first()
                if part_location is None:
                    part_location = PartLocation(part=part, location=location, quantity=0, primary_location=False)
                    part_location.save()

                if len(qty) > 0:
                    part_location.quantity = int(qty)

                if len(barcode) > 0:
                    part_location.barcode = barcode

                if len(primary) > 0:
                    part_location.primary_location = bool(primary)

                part_location.save()

            part.save()

            print('\r', end='')
