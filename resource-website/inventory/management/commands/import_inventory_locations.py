import argparse
import csv
from django.core.management.base import BaseCommand

from inventory.models import Location


class Command(BaseCommand):
    help = (
        "Imports inventory locations from a csv file in the format \n"
        "path | non local\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')

            locations = list(filter(lambda c: len(c) > 0, row[0].split('/')))
            non_local = row[1]

            location = None
            for location_name in locations:
                current_location = Location.objects.filter(name=location_name, parent=location).first()
                if current_location is None:
                    current_location = Location(name=location_name, parent=location, non_local=True)
                    current_location.save()

                location = current_location

            if len(non_local) > 0:
                location.non_local = bool(non_local)
                location.save()

            print('\r', end='')
