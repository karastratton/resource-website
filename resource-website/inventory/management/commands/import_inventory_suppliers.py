import argparse
import csv
from django.core.management.base import BaseCommand

from inventory.models import Supplier


class Command(BaseCommand):
    help = (
        "Imports inventory locations from a csv file in the format \n"
        "path | non local\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')

            name = row[0]
            website = row[1]

            supplier = Supplier.objects.filter(name=name).first()
            if supplier is None:
                supplier = Supplier(name=name, website_url=website)

            if len(website) > 0:
                supplier.website_url = website

            supplier.save()

            print('\r', end='')
