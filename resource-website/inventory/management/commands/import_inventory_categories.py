import argparse
import csv
from django.core.management.base import BaseCommand, CommandError

from inventory.models import Category, Property


class Command(BaseCommand):
    help = (
        "Imports inventory categories from a csv file in the format \n"
        "path | part name template | property 1 | property 2 | .. | property n\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')

            categories = list(filter(lambda c: len(c) > 0, row[0].split('/')))
            template = row[1]
            properties = list(filter(lambda p: len(p) > 0, row[2:]))

            category = None
            for category_name in categories:
                current_category = Category.objects.filter(name=category_name, parent=category).first()
                if current_category is None:
                    current_category = Category(name=category_name, parent=category)
                    current_category.save()

                category = current_category

            if len(template) > 0:
                category.part_name_template = template
                category.save()

            for property_name in properties:
                name_and_unit = property_name.split('=')
                name = name_and_unit[0]
                unit = name_and_unit[1] if len(name_and_unit) > 1 else None
                metric = name_and_unit[2] if len(name_and_unit) > 2 else None
                property = Property.objects.filter(name=name, category=category).first()
                if property is None:
                    property = Property(name=name, category=category, metric=False)
                    property.save()

                property.unit = unit

                if metric == 'metric':
                    property.metric = True
                elif metric is not None:
                    property.metric = False

                property.save()

            print('\r', end='')
