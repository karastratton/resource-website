import re

units = {
    'yotta': 1e24,
    'Y': 1e24,
    'zetta': 1e21,
    'Z': 1e21,
    'exa': 1e18,
    'E': 1e18,
    'peta': 1e15,
    'P': 1e15,
    'tera': 1e12,
    'T': 1e12,
    'giga': 1e9,
    'G': 1e9,
    'mega': 1e6,
    'M': 1e6,
    'kilo': 1e3,
    'Kilo': 1e3,
    'k': 1e3,
    'K': 1e3,
    #'hecto': 1e2,
    #'Hecto': 1e2,
    #'h': 1e2,
    #'H': 1e2,
    #'deca': 1e1,
    #'Deca': 1e1,
    #'da': 1e1,
    #'Da': 1e1,
    #'deci': 1e-1,
    #'Deci': 1e-1,
    #'d': 1e-1,
    #'D': 1e-1,
    #'centi': 1e-2,
    #'Centi': 1e-2,
    #'c': 1e-2,
    #'C': 1e-2,
    'milli': 1e-3,
    'Milli': 1e-3,
    'm': 1e-3,
    'micro': 1e-6,
    'Micro': 1e-6,
    'u': 1e-6,
    'U': 1e-6,
    # Greek Small Letter Mu (0x03BC)
    'μ': 1e-6,
    # Micro Sign (0x00B5)
    'µ': 1e-6,
    'nano': 1e-9,
    'Nano': 1e-9,
    'n': 1e-9,
    'N': 1e-9,
    'pico': 1e-12,
    'Pico': 1e-12,
    'p': 1e-12,
    'femto': 1e-15,
    'Femto': 1e-15,
    'f': 1e-15,
    'F': 1e-15,
    'atto': 1e-18,
    'Atto': 1e-18,
    'a': 1e-18,
    'A': 1e-18,
    'zepto': 1e-21,
    'Zepto': 1e-21,
    'z': 1e-21,
    'yocto': 1e-24,
    'Yocto': 1e-24,
    'y': 1e-24,
}


def parse_metric(value: str) -> str:
    p = re.compile(r'(([1-9][0-9]*\.?[0-9]*)|(\.[0-9]+))[ ]*([a-zA-Zµμ]*)')
    matches = p.findall(value)
    if len(matches) >= 1 and len(matches[0]) == 4:
        number = float(matches[0][0])
        unit = str(matches[0][3])
        if len(unit) > 0:
            for name, amount in units.items():
                if unit.startswith(name):
                    number *= amount
                    break

        return str(number)
    else:
        return value


def render_metric(value: str) -> str:
    try:
        num = float(value)
    except:
        num = None

    if num is not None:

        unit_past_one = min(filter(lambda u: u[1] > 1, units.items()), key=lambda u: u[1])

        if 1 <= num < unit_past_one[1]:
            scaled_num = num
            prefix = ''
        else:
            unit_smaller = None

            for name, scale in reversed(list(units.items())):
                if unit_smaller is None:
                    unit_smaller = ([name], scale)
                else:
                    if scale == unit_smaller[1]:
                        unit_smaller[0].append(name)
                    elif scale <= num and  scale > unit_smaller[1]:
                        unit_smaller = ([name], scale)

            scaled_num = num / unit_smaller[1]
            prefix = min(unit_smaller[0], key=lambda p: len(p) + (0 if p.islower() else 1));

        return "{} {}".format(round(scaled_num, 4), prefix)

    else:
        return value


def clean_metric(value: str) -> str:
    return render_metric(parse_metric(value))
