from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_index, name='inventory'),
    path('view_category/<int:category_id>', views.view_category, name='parts_category'),
    path('view_category/<int:category_id>/page/<int:page>/<order>', views.view_category, name='parts_category_page'),
    path('view_category/<int:category_id>/locations', views.view_part_locations, name='part_locations'),
    path('part/<int:part_id>', views.view_part, name='part'),
    path('cart', views.view_cart, name='cart'),
    path('previous_orders', views.view_previous_orders, name='previous_orders'),
    path('digikey_auth', views.view_digikey_auth, name='digikey_auth'),
    path('digikey_lookup', views.view_digikey_lookup, name='digikey_lookup'),
    path('barcodes', views.view_barcodes, name='barcodes'),
    path('barcode_lookup', views.view_barcode_lookup, name='barcode_lookup'),
    path('fillorders', views.fill_orders, name='fill_orders'),
    path('fillorders/<int:order_id>', views.fill_order, name='fill_order'),
    path('submit_request', views.view_submit_request, name='submit_request'),
    path('analytics_categories', views.view_analytics_categories, name='analytics_categories'),
    path('analytics_parts', views.view_analytics_parts, name='analytics_parts'),
]
