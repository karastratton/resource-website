from string import Formatter

from django.db import models
from django.db.models import Sum

from user_management.models import CustomUser

from .metric import *
from .fields import NaturalSortField


class Manufacturer(models.Model):
    name = models.CharField(max_length=512)
    website_url = models.URLField()

    def __str__(self):
        return "{} ({})".format(self.name, self.id)


def category_picture(category, filename):
    return 'uploads/inventory/category/{}/{}'.format(category.id, filename)


class Category(models.Model):
    name = models.CharField(max_length=512)
    parent = models.ForeignKey("Category", on_delete=models.PROTECT, null=True, blank=True)
    part_name_template = models.CharField(max_length=512, blank=True, null=True)
    image = models.ImageField(null=True, blank=True, upload_to=category_picture)

    def __str__(self):
        path = [self] + self.get_all_parentcategories()
        path.reverse()
        path = map(lambda c: c.name, path)
        return "/".join(path)

    class Meta:
        verbose_name = "category"
        verbose_name_plural = "categories"

    def get_all_subcategories(self):
        subcategories = []
        for subcategory in Category.objects.filter(parent=self):
            subcategories.append(subcategory)
            subcategories += subcategory.get_all_subcategories()

        return subcategories

    def get_all_parentcategories(self):
        parentcategories = []
        if self.parent is not None:
            parentcategories.append(self.parent)
            parentcategories += self.parent.get_all_parentcategories()
        return parentcategories

    def properties_q(self):
        q = models.Q(category=self)
        if self.parent is not None:
            q = q | self.parent.properties_q()

        return q

    def parts_q(self):
        q = models.Q(category=self)
        for subcategory in Category.objects.filter(parent=self):
            q = q | subcategory.parts_q()

        return q

    def parts_count(self):
        return sum(map(lambda c: c.part_set.count(), self.get_all_subcategories() + [self]))

    def part_locations_count(self):
        return sum(map(lambda p: p.partlocation_set.count(), Part.objects.filter(self.parts_q())))

    def parts_quantity(self):
        return sum(map(lambda p: sum(map(lambda pl: pl.quantity, p.partlocation_set.all())),
                       Part.objects.filter(self.parts_q())))

    def parts_price(self):
        return sum(map(
            lambda p: (p.nominal_price() or 0) * sum(map(
                lambda pl: pl.quantity,
                p.partlocation_set.all()
            )),
            Part.objects.filter(self.parts_q())
        ))

    def parts_ordered(self):
        return sum(map(
            lambda p: sum(map(
                lambda po: po.quantity,
                p.partorder_set.all()
            )),
            Part.objects.filter(self.parts_q())
        ))


class Supplier(models.Model):
    name = models.CharField(max_length=512)
    website_url = models.URLField()

    def __str__(self):
        return "{} ({})".format(self.name, self.id)


class Location(models.Model):
    name = models.CharField(max_length=512)
    parent = models.ForeignKey("Location", on_delete=models.PROTECT, null=True, blank=True)
    non_local = models.BooleanField(null=False)

    def __str__(self):
        path = [self] + self.parents()
        path.reverse()
        path = map(lambda l: l.name, path)
        return "/".join(path)

    def parents(self):
        parents = []
        if self.parent is not None:
            parents.append(self.parent)
            parents += self.parent.parents()
        return parents


class Property(models.Model):
    name = models.CharField(max_length=512, blank=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    unit = models.CharField(max_length=16, blank=True, null=True)
    metric = models.BooleanField()

    class Meta:
        verbose_name = "property"
        verbose_name_plural = "properties"

    def __str__(self):
        return "{} - {} ({})".format(self.category.name, self.name, self.id)

    def render(self, value):
        if self.metric:
            value = render_metric(value)

        if self.unit is not None:
            value += self.unit

        return value


class PartStatus(models.TextChoices):
    ACTIVE = "ACTIVE", "Active"
    NONSTOCK = "NONSTOCK", "Non-stock"
    OBSOLETE = "OBSOLETE", "Obsolete"


class Part(models.Model):
    # If name is null, the part name template from the category should be used
    name_override = models.CharField(max_length=512, null=True, blank=True)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.SET_NULL, null=True)
    manufacturer_part_number = models.CharField(max_length=512, null=False, blank=True)
    supplier = models.ManyToManyField(Supplier, through='PartSupplier')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    datasheet = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    locations = models.ManyToManyField(Location, through='PartLocation')
    properties = models.ManyToManyField(Property, through='PartProperty')
    status = models.CharField(
        max_length=512,
        choices=PartStatus.choices,
        default=PartStatus.ACTIVE,
    )

    def __str__(self):
        return "{} ({})".format(self.name, self.id)

    class Meta:
        permissions = (
            ("use_inventory", "Can use inventory"),
            ("edit_parts", "Can edit parts"),
        )

    def part_name_template(self):
        if self.category.part_name_template is not None:
            return self.category.part_name_template
        else:
            parents = self.category.get_all_parentcategories()
            for parent in parents:
                if parent.part_name_template is not None:
                    return parent.part_name_template
            return None

    def status_label(self):
        return dict(PartStatus.choices)[self.status]

    @property
    def name(self):
        if self.name_override is not None:
            return self.name_override
        else:
            properties = PartProperty.objects.filter(part=self)
            keys = {}
            for property in properties:
                value = property.value
                if property.property.metric:
                    value = render_metric(value)
                if property.property.unit is not None:
                    keys[property.property.name] = value + property.property.unit
                else:
                    keys[property.property.name] = value

            part_name_template = self.part_name_template()

            if part_name_template is not None:
                for text, key, spec, conversion in Formatter().parse(part_name_template):
                    if key is not None and key not in keys:
                        keys[key] = "―"

                return self.part_name_template().format(**keys)
            else:
                return "―"

    @property
    def qty_available(self):
        locations = PartLocation.objects.filter(part=self)
        stock = 0
        for part_location in locations:
            stock += part_location.quantity
        return stock

    def nominal_price(self):
        part_supplier = PartSupplier.objects.filter(part=self, price__isnull=False).first()

        if part_supplier is not None:
            return part_supplier.price
        else:
            return None

    def quantity_ordered(self):
        return PartOrder.objects.filter(part=self).aggregate(Sum('quantity'))['quantity__sum'] or 0


class PartSupplier(models.Model):
    part = models.ForeignKey(Part, on_delete=models.CASCADE)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)
    supplier_part_number = models.CharField(max_length=512, null=True, blank=True)
    supplier_part_url = models.URLField(blank=True, null=True)
    price = models.FloatField(blank=True, null=True)

    def __str__(self):
        return "{} - {} ({})".format(self.part.name, self.supplier.name, self.id)

    class Meta:
        permissions = (
            ("view_part_suppliers", "View Part Suppliers"),
        )


class PartProperty(models.Model):
    part = models.ForeignKey(Part, on_delete=models.CASCADE)
    property = models.ForeignKey(Property, on_delete=models.CASCADE)
    value = models.CharField(max_length=512)
    value_sort = NaturalSortField(for_field='value', max_length=512)

    class Meta:
        verbose_name = "part property"
        verbose_name_plural = "part properties"

    def __str__(self):
        return "{} - {} ({})".format(self.part.name, self.property, self.id)

    def __lt__(self, other):
        try:
            first = float(self.value)
            second = float(other.value)
            return first.__lt__(second)
        except ValueError:
            return self.value.__lt__(other)

    def rendered(self):
        return self.property.render(self.value)


class PartLocation(models.Model):
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    part = models.ForeignKey(Part, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    barcode = models.CharField(max_length=512, null=True, blank=True)
    primary_location = models.BooleanField(null=False)

    def __str__(self):
        return "{} - {} ({})".format(self.part.name, self.location.name, self.id)

    class Meta:
        permissions = (
            ('generate_barcode_sheets', 'Generate barcode sheets'),
            ('lookup_barcodes', "Lookup barcodes"),
            ('view_part_locations', "View Part Locations"),
        )


class OrderStatus(models.TextChoices):
    CART = "CART", "Cart"
    SUBMITTED = "SUBMITTED", "Submitted"
    PROCESSING = "PROCESSING", "Processing"
    COMPLETE = "COMPLETE", "Complete"
    PICKED_UP = "PICKED_UP", "Picked Up"
    CANCELLED = "CANCELLED", "Cancelled"
    REJECTED = "REJECTED", "Rejected"


class Order(models.Model):
    status = models.CharField(
        max_length=512,
        choices=OrderStatus.choices,
        default=OrderStatus.CART,
    )
    parts = models.ManyToManyField(Part, through='PartOrder')
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)

    def status_label(self):
        return dict(OrderStatus.choices)[self.status]

    class Meta:
        permissions = (
            ("fill_orders", "Can fill orders"),
        )


class PartOrder(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    part = models.ForeignKey(Part, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(null=False)
    fulfilled = models.ManyToManyField(PartLocation, through='PartOrderFulfillment')

    @property
    def fulfilled_quantity(self):
        quantity = self.partorderfulfillment_set.aggregate(Sum('quantity'))['quantity__sum']
        return quantity if quantity is not None else 0


class PartOrderFulfillment(models.Model):
    part_order = models.ForeignKey(PartOrder, on_delete=models.CASCADE)
    part_location = models.ForeignKey(PartLocation, on_delete=models.SET_NULL, null=True)
    quantity = models.PositiveIntegerField()


class OrderStateChanges(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    last_state = models.CharField(max_length=512, choices=OrderStatus.choices)
    next_state = models.CharField(max_length=512, choices=OrderStatus.choices)
    timestamp = models.DateTimeField()
    changed_by = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = "order state change"
        verbose_name_plural = "order state changes"


class RequestStatus(models.TextChoices):
    SUBMITTED = "SUBMITTED", "Submitted"
    COMPLETE = "COMPLETE", "Complete"
    CANCELLED = "CANCELLED", "Cancelled"
    REJECTED = "REJECTED", "Rejected"


class Request(models.Model):
    status = models.CharField(
        max_length=512,
        choices=RequestStatus.choices,
        default=RequestStatus.SUBMITTED,
    )
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)
    request = models.TextField()
    notes = models.TextField()

    def status_label(self):
        return dict(RequestStatus.choices)[self.status]

    class Meta:
        permissions = (
            ("fill_requests", "Can fill requests"),
        )


class DigiKeyAuth(models.Model):
    access_token = models.CharField(max_length=512)
    refresh_token = models.CharField(max_length=512)
    expired_in = models.PositiveIntegerField()
    refresh_token_expires_in = models.PositiveIntegerField()
    timestamp = models.DateTimeField()
    auto_refreshed = models.BooleanField()
    user = models.ForeignKey(CustomUser, on_delete=models.SET_NULL, null=True)

    class Meta:
        permissions = (
            ("digikey_auth", "Can authenticate Digi-Key"),
            ("digikey_import", "Can import from Digi-Key"),
        )
