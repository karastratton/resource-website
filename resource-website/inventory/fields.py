from django.db import models
import re


# See https://github.com/niall-byrne/django-naturalsortfield
class NaturalSortField(models.CharField):
    def __init__(self, for_field, **kwargs):
        self.for_field = for_field
        kwargs.setdefault('db_index', True)
        kwargs.setdefault('editable', False)
        kwargs.setdefault('max_length', 255)
        super(NaturalSortField, self).__init__(**kwargs)
        self.max_length = kwargs['max_length']

    def deconstruct(self):
        name, path, args, kwargs = super(NaturalSortField, self).deconstruct()
        args.append(self.for_field)
        return name, path, args, kwargs

    def pre_save(self, model_instance, add):
        return self.naturalize(getattr(model_instance, self.for_field))

    def naturalize(self, string):
        def naturalize_int_match(match):
            # Formatting numbers with all the leading and trailing zeros
            # makes an alphabetic sort work nicely with numbers
            return '{:060.30f}'.format(float(match.group(0)))

        string = string.lower()
        string = string.strip()
        string = re.sub(r'^the\s+', '', string)
        string = re.sub(r'[-+]?[0-9]*\.?[0-9]+', naturalize_int_match, string)
        string = string[:self.max_length]

        return string