from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_index, name='printcenter'),
    path('printrequest', views.view_print_request, name='print_request'),
    path('printview/<int:print_id>', views.view_print_view, name='print_view'),
    path('printqueue', views.view_print_queue, name='print_queue'),
    path('printqueuecomplete', views.view_print_queue_complete, name='print_queue_complete'),
    path('printedit/<int:print_id>', views.view_print_edit, name='print_edit'),

]
