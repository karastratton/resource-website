from django.apps import AppConfig


class PrintcenterConfig(AppConfig):
    name = 'printcenter'
