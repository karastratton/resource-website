import random
from django import forms
from django.forms import inlineformset_factory

from .models import PrintSize, PrintUse, PrintMaterial, PrintMaterialColor, PrintFilamentSpool, Printer, PrintStatus, PrintRequest


class PrintRequestForm(forms.ModelForm):
    class Meta:
        model = PrintRequest
        fields = ['material', 'color', 'size', 'print_use', 'file', 'notes']


class PrintEditForm(forms.ModelForm):
    class Meta:
        model = PrintRequest
        fields = ['material', 'color', 'spool', 'printer', 'print_status', 'print_weight', 'size', 'file', 'notes']
