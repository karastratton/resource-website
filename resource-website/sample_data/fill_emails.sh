#!/bin/bash

# Fill in the email field of a users csv from the first and last
# names and a random number matching rowan's current email format.
# uses the same csv format as the import_users script
# firstname, lastname, bannerid, email, group1, group2, ..
# but it only cares about the firstname, lastname, and email

# first argument is file to read from, second is file to write to
# call like
# `sh fill_emails.sh users1.csv users2.csv`

cat $1 | awk -F ',' -v OFS=',' '{ $4=tolower($2) substr(tolower($1), 1, 1) int(rand()*100) "@students.rowan.edu"; print }' > $2