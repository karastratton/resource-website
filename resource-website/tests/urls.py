from django.urls import path

from . import views

urlpatterns = [
    path('', views.view_index, name='tests'),

    path('tests/<int:test_id>', views.view_test_sessions, name='test_sessions'),
    path('tests/<int:test_id>/details', views.view_test_detail, name='edit_test'),
    path('tests/<int:test_id>/summary', views.view_test_result, name='test_summary'),
    path('tests/<int:test_id>/create_session', views.view_create_session, name='create_session'),

    path('sessions/<int:session_id>', views.view_test_session, name='test_session'),
    path('sessions/<int:session_id>/card_swipe_in', views.view_card_swipe_in, name='card_swipe_in'),
    path('sessions/<int:session_id>/card_swipe_out', views.view_card_swipe_out, name='card_swipe_out'),

    path('registrations/<int:registration_id>', views.view_registration, name='registrations'),

    path('registrations/<int:registration_id>/open', views.view_take_test, name='take_test'),

    path('create_test', views.view_create_test, name='create_test'),
    path('create_category', views.view_create_category, name='create_category'),

    path('categories', views.view_categories , name='categories'),
    path('categories/<int:category_id>', views.view_category, name="category"),
    path('categories/<int:category_id>/questions/<int:question_id>', views.view_question, name="question"),
    path('categories/<int:category_id>/create_question', views.view_create_question, name="create_question"),
]
