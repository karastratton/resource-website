# Generated by Django 2.2.7 on 2019-12-03 01:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0008_auto_20191202_1932'),
    ]

    operations = [
        migrations.RenameField(
            model_name='registrationquestion',
            old_name='test_session',
            new_name='registration',
        ),
        migrations.AddField(
            model_name='registration',
            name='session',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='tests.Session'),
            preserve_default=False,
        ),
    ]
