# Generated by Django 3.1 on 2020-08-31 14:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trainings', '0009_auto_20200831_1015'),
        ('tests', '0035_auto_20200624_2219'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttemptTraining',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attempt', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tests.attempt')),
                ('user_training', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='trainings.usertraining')),
            ],
        ),
    ]
