# Generated by Django 3.0.3 on 2020-03-17 18:36

from django.db import migrations, models
import tests.models


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0021_auto_20200303_2300'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='image',
            field=models.ImageField(blank=True, null=True, upload_to=tests.models.answer_image_upload_to),
        ),
    ]
