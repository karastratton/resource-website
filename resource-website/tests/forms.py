from django import forms
from django.forms import inlineformset_factory, modelformset_factory, DateInput

from crispy_forms.layout import Div

from pages.crispy import InputGroupLayout, DynamicFormsetHelper
from .models import Category
from .models import Question
from .models import Session
from .models import Answer
from .models import AttemptQuestion
from .models import TestCategory
from .models import TrainingTest


class AnswerField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.text


class AttemptQuestionForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['selected_answer'] = AnswerField(
            queryset=Answer.objects.filter(question=self.instance.question).order_by('?'),
            empty_label=None,
            label=self.instance.question.question_text,
            widget=forms.RadioSelect
        )

    class Meta:
        model = AttemptQuestion
        fields = ('selected_answer',)


TakeTestForm = modelformset_factory(
    AttemptQuestion,
    form=AttemptQuestionForm,
    extra=0,
)


def populate_test_form_images(formset):
    # Go through all the BoundWidgets for the answers for all the questions in the test and add an image field so
    # the template can render the image
    for form in formset:
        field = form['selected_answer']
        for option in field:
            id = option.data['value']
            if hasattr(id, 'value'):
                id = id.value
            answer = Answer.objects.get(pk=id)
            option.image = answer.image


class CreateTestForm(forms.Form):
    name = forms.CharField()
    passing_score = forms.FloatField(max_value=100, min_value=0)


class TestCategoryForm(forms.ModelForm):
    class Meta:
        model = TestCategory
        fields = ['category', 'number_of_questions']


TestCategoryFormset = modelformset_factory(
    TestCategory,
    form=TestCategoryForm,
    extra=1,
    #min_num=1,
    #validate_min=True,
)


class TestCategoryFormsetHelper(DynamicFormsetHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(
            form_layout=InputGroupLayout(
                'category',
                'number_of_questions',
                css_class="mb-3"
            ),
            name='Categories',
            prefix='categories',
            *args,
            **kwargs
        )


class TrainingTestForm(forms.ModelForm):
    class Meta:
        model = TrainingTest
        fields = ['training', 'expiration']
        widgets = {
            'expiration': DateInput(attrs={'type': 'date'})
        }


TrainingTestFormset = modelformset_factory(
    TrainingTest,
    form=TrainingTestForm,
    extra=1,
)


class TrainingTestFormsetHelper(DynamicFormsetHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(
            form_layout=InputGroupLayout(
                'training',
                'expiration',
                css_class="mb-3"
            ),
            name='Trainings',
            prefix='trainings',
            *args,
            **kwargs
        )


class CreateSessionForm(forms.ModelForm):
    class Meta:
        model = Session
        fields = ['name', 'attempts', 'max_registrations', 'card_swipes_required']


class CreateCategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['name']


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['question_text', 'image']


AnswersFormSet = inlineformset_factory(Question, Answer, fields=('text', 'image', 'correct'), extra=1)


class CardSwipeForm(forms.Form):
    card_uid = forms.CharField(
        strip=True,
        widget=forms.TextInput(attrs={'autofocus': True})
    )
