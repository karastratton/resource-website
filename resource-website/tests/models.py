import random

import logging

from django.db import models
from django.conf import settings
from django.db.models import OneToOneField, ForeignKey
from django.utils import timezone

from trainings.models import Training, UserTraining

log = logging.getLogger(__name__)


class Category(models.Model):
    name = models.CharField(max_length=512, default="")
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "{} ({})".format(self.name, self.id)

    class Meta:
        verbose_name = "question category"
        verbose_name_plural = "question categories"


class Test(models.Model):
    name = models.CharField(max_length=512, default="")
    categories = models.ManyToManyField(Category, through='TestCategory')
    passing_score = models.FloatField()
    trainings = models.ManyToManyField(Training, through='TrainingTest')
    visible = models.BooleanField(default=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return "{} ({})".format(self.name, self.id)

    class Meta:
        permissions = (
            ("take_test", "Can take test"),
            ("view_test_result", "View test result"),
            ("create_test", "Can create test"),
        )


class TestCategory(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    number_of_questions = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = "test question category"
        verbose_name_plural = "test question categories"


class TrainingTest(models.Model):
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    training = models.ForeignKey(Training, on_delete=models.CASCADE)
    expiration = models.DateField()

    class Meta:
        verbose_name = "associated training"
        verbose_name_plural = "associated trainings"


def answer_image_upload_to(answer, filename):
    return 'uploads/tests/questions/{}/answers/{}/{}'.format(answer.question.id, answer.id, filename)


class Answer(models.Model):
    text = models.CharField(max_length=512, default="")
    image = models.ImageField(null=True, blank=True, upload_to=answer_image_upload_to)
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    correct = models.BooleanField(default=False)

    def __str__(self):
        return "{} ({})".format(self.text, self.id)


def question_image_upload_to(question, filename):
    return 'uploads/tests/questions/{}/{}'.format(question.id, filename)


class Question(models.Model):
    question_text = models.CharField(max_length=512, default="")
    image = models.ImageField(null=True, blank=True, upload_to=question_image_upload_to)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return "{} ({})".format(self.question_text, self.id)


class Session(models.Model):
    name = models.CharField(max_length=512, default="")
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    max_registrations = models.PositiveIntegerField(null=True)
    attempts = models.PositiveSmallIntegerField(default=1)
    registrations_enabled = models.BooleanField(default=False)
    opening_enabled = models.BooleanField(default=False)
    card_swipes_required = models.BooleanField(default=True)
    proctors = models.ManyToManyField(settings.AUTH_USER_MODEL)
    # There used to be opening and submission enabling, they have now been combined into the same variable.
    # There are places in the code that call for submission enabled, so it has been put here for sanity
    # submissions_enabled = models.BooleanField(default=False)

    # There are places in the code that call for submission enabled, so it has been put here for sanity
    def submissions_enabled(self):
        return self.opening_enabled

    def __str__(self):
        return "{} ({})".format(self.name, self.id)

    class Meta:
        permissions = (
            ("proctor_session", "Can proctor session"),
        )

    def registration_status(self, user):
        # This is atrocious, but I don't know of a better way to do it, partially because python
        # Would be nice to at least have proper enums that can be used in the templates instead of these strings
        # I recommend copy/pasting the strings right out of here instead of typing them out to avoid typos
        # There is no good was to ensure that all of the usages of the function check for a valid string
        # - Tim H

        if self.registration_set.filter(taker=user).count() > 0:
            registration = self.registration_set.get(taker=user)

            # Registered and passed
            if any(map(lambda attempt: attempt.passed(), registration.attempt_set.all())):
                return "REGISTERED_FOR_THIS_SESSION_PASSED"

            # Registered, but used up all retries
            if registration.attempt_set.filter(submitted__isnull=False).count() >= self.attempts:
                return "REGISTERED_FOR_THIS_SESSION_USED_RETRIES"

            # Registered, with no open attempts, but opening disabled
            if registration.attempt_set.filter(submitted__isnull=True).count() == 0 and not self.opening_enabled:
                return "REGISTERED_FOR_THIS_SESSION_NOT_OPEN_OPENING_DISABLED"

            # Make sure they swiped in with their Rowan Card...
            if registration.swiped_in is None and registration.session.card_swipes_required == True:
                return "REGISTERED_FOR_THIS_SESSION_NOT_SWIPED_IN"

            # ...but not swiped out
            if registration.swiped_out is not None and registration.session.card_swipes_required == True:
                return "REGISTERED_FOR_THIS_SESSION_SWIPED_OUT"

            # Has an open attempt...
            if registration.attempt_set.filter(submitted__isnull=True).count() > 0:
                # ...and can submit
                if self.submissions_enabled:
                    return "REGISTERED_FOR_THIS_SESSION_OPENED_SUBMISSIONS_ENABLED"

                # ...and cannot submit
                else:
                    return "REGISTERED_FOR_THIS_SESSION_OPENED_SUBMISSIONS_DISABLED"

            # Does not have an opened attempt, and opening is enabled
            return "REGISTERED_FOR_THIS_SESSION_NOT_OPEN_OPENING_ENABLED"

        else:
            # Not registered, and registration not enabled
            if not self.registrations_enabled:
                return "REGISTRATION_CLOSED"

            # Not registered, and session full
            if self.max_registrations is not None and self.registration_set.count() >= self.max_registrations:
                return "SESSION_FULL"

            # Not registered, and registered for another session of the same test without completing it
            # Find all the registrations that count as open. These are for this test, with this user, that have not
            # been submitted, and have not been swiped out of.
            if Registration \
                    .objects \
                    .filter(session__test=self.test) \
                    .filter(taker=user) \
                    .filter(attempt__submitted__isnull=True) \
                    .filter(swiped_out__isnull=True) \
                    .count() > 0:
                return "REGISTERED_FOR_ANOTHER_SESSION"

            # Not registered, and can register for the session
            return "CAN_REGISTER"

    def register(self, user):
        if self.registration_status(user) == "CAN_REGISTER":
            registration = Registration(session=self, taker=user)
            registration.save()
        else:
            log.error("{} attempted to register for session {} with status {} that cannot be registered for".format(user,
                      self, self.registration_status(user)))

    def unregister(self, user):
        status = self.registration_status(user)
        if status == "REGISTERED_FOR_THIS_SESSION_NOT_SWIPED_IN" or status == "REGISTERED_FOR_THIS_SESSION_NOT_OPEN_OPENING_DISABLED":
            registration = Registration.objects.get(session=self, taker=user)
            registration.delete()
        else:
            log.error("{} attempted to unregister for session {} with status {} that cannot be unregistered from".format(user,
                      self, self.registration_status(user)))


class Registration(models.Model):
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    taker = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    registered = models.DateTimeField(default=timezone.now)
    swiped_in = models.DateTimeField(null=True, blank=True)
    swiped_out = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return "{} for {} ({})".format(self.session, self.taker, self.id)

    def open(self, user):
        if self.session.registration_status(user) == "REGISTERED_FOR_THIS_SESSION_NOT_OPEN_OPENING_ENABLED":
            attempt = Attempt(registration=self)
            attempt.save()

            attempt_questions = []

            for test_category in TestCategory.objects.filter(test=self.session.test):
                number_of_questions = test_category.number_of_questions
                if number_of_questions > 0:
                    questions = Question.objects.filter(category=test_category.category)[:number_of_questions]
                    for question in questions:
                        attempt_question = AttemptQuestion(
                            attempt=attempt,
                            question=question,
                        )

                        attempt_questions.append(attempt_question)

            random.shuffle(attempt_questions)

            for attempt_question in attempt_questions:
                attempt_question.save()

            return attempt
        else:
            log.error("{} attempted to open registration {} with status {} that cannot be opened".format(user, self,
                                                                                                         self.session.registration_status(
                                                                                                             user)))


class Attempt(models.Model):
    registration = models.ForeignKey(Registration, on_delete=models.CASCADE)
    opened = models.DateTimeField(default=timezone.now)
    submitted = models.DateTimeField(null=True, blank=True)
    questions = models.ManyToManyField(Question, through='AttemptQuestion')
    score = models.FloatField(null=True, blank=True)

    def __str__(self):
        return "{} at {} ({})".format(self.registration, self.opened, self.id)

    def grade(self):
        if self.opened is None or self.submitted is None:
            return None

        if self.score is not None:
            return self.score

        total_questions = 0
        correct_questions = 0
        for attempt_question in AttemptQuestion.objects.filter(attempt=self):
            total_questions = total_questions + 1
            if attempt_question.selected_answer is not None and attempt_question.selected_answer.correct:
                correct_questions = correct_questions + 1

        if total_questions == 0:
            return 0.0

        self.score = correct_questions / total_questions

        self.save()

        return self.score

    def passed(self):
        grade = self.grade()
        if grade is None:
            return None
        else:
            return self.grade() >= self.registration.session.test.passing_score
    passed.boolean = True


class AttemptQuestion(models.Model):
    attempt = models.ForeignKey(Attempt, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    selected_answer = models.ForeignKey(Answer, on_delete=models.CASCADE, null=True)


class AttemptTraining(models.Model):
    attempt = ForeignKey(Attempt, on_delete=models.CASCADE)
    user_training = ForeignKey(UserTraining, on_delete=models.CASCADE)