import argparse
import csv
from django.core.management.base import BaseCommand, CommandError

from tests.models import Category, Question, Answer


class Command(BaseCommand):
    help = (
        "Imports questions and question categories from a csv file in the format \n"
        "category name | question text | correct answer | incorrect answer 1 | incorrect answer 2 | ...\n"
        "There can be any number of incorrect answers. Categories will be created if they do not already exist"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')
            category_text = row[0]
            category = Category.objects.filter(name=category_text).first()

            if category is None:
                category = Category(name=category_text)

            category.save()

            question_text = row[1]
            question = Question(question_text=question_text, category=category)
            question.save()

            correct_answer_text = row[2]
            correct_answer = Answer(text=correct_answer_text, question=question, correct=True)
            correct_answer.save()

            for incorrect_answer_text in row[3:]:
                if len(incorrect_answer_text) > 0:
                    incorrect_answer = Answer(text=incorrect_answer_text, question=question, correct=False)
                    incorrect_answer.save()

            print('\r', end='')
