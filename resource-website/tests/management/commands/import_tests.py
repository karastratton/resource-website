import argparse
import csv
from django.core.management.base import BaseCommand, CommandError

from tests.models import Category, Test, TestCategory


class Command(BaseCommand):
    help = (
        "Imports tests and test question categories from a csv file in the format \n"
        "test name | category name | number of questions\n"
    )

    def add_arguments(self, parser):
        parser.add_argument('csv_file', nargs=1, type=argparse.FileType('rt'))

    def handle(self, *args, **options):
        file = options['csv_file'][0]
        reader = csv.reader(file)
        for i, row in enumerate(reader):
            print("Importing {: 6}".format(i), end='')
            test_name = row[0]
            test = Test.objects.filter(name=test_name).first()
            if test is None:
                test = Test(name=test_name, passing_score=0.8)
            else:
                test.passing_score = 0.8
            test.save()

            category_name = row[1]
            category = Category.objects.filter(name=category_name).first()
            if category is None:
                category = Category(name=category_name)
            category.save()

            number_of_questions = row[2]
            test_category = TestCategory.objects.filter(test=test, category=category).first()
            if test_category is None:
                test_category = TestCategory(test=test, category=category, number_of_questions=number_of_questions)
            else:
                test_category.number_of_questions=number_of_questions
            test_category.save()
            print('\r', end='')

