from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Div


def add_class(f, c=None):
    if c is not None:
        if hasattr(f, 'css_class') and f.css_class is not None:
            f.css_class += " {}".format(c)
        else:
            f.css_class = c

    return f


class InputGroupLayout(Layout):

    def __init__(self, *args, **kwargs):

        prepend = kwargs.pop('prepend') if 'prepend' in kwargs else []
        append = kwargs.pop('append') if 'append' in kwargs else []

        made_fields = list(map(lambda f: add_class(f, c='input-group-prepend'), prepend)) \
                      + list(map(lambda f: Field(f, template='pages/input_group_layout.html'), args)) \
                      + list(map(lambda f: add_class(f, c='input-group-append'), append))

        if 'css_class' in kwargs:
            kwargs['css_class'] += ' input-group'
        else:
            kwargs['css_class'] = ' input-group'

        layout = Div(
            *made_fields,
            **kwargs
        )

        super().__init__(layout)

    def append(self, layout):
        layout = add_class(layout, 'input-group-append')
        self.fields[0].append(layout)


class DynamicFormsetHelper(FormHelper):
    def __init__(self, form_layout, name, prefix, *args, **kwargs):
        super().__init__(*args, **kwargs)

        form_layout.append(Div(css_class='{}_remove'.format(prefix)))

        self.name = name
        self.prefix = prefix
        self.template = 'pages/dynamic_formset.html'
        self.layout = Div(
            form_layout,
            css_class=prefix,
        )
