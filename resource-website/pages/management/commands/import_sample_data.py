
from django.core.management.base import BaseCommand

from user_management.management.commands import import_users
from tests.management.commands import import_questions, import_tests
from inventory.management.commands import import_inventory_categories, import_inventory_locations, import_inventory_manufacturers, import_inventory_suppliers, import_inventory_parts, import_inventory_orders


class Command(BaseCommand):
    help = "Imports all sample data"

    def handle(self, *args, **options):
        print("Importing users")
        import_users.Command().handle(csv_file=[open('sample_data/users.csv', 'r')])

        print("Importing questions")
        import_questions.Command().handle(csv_file=[open('sample_data/questions.csv', 'r')])

        print("Importing tests")
        import_tests.Command().handle(csv_file=[open('sample_data/tests.csv', 'r')])

        print("Importing Inventory Categories")
        import_inventory_categories.Command().handle(csv_file=[open('sample_data/inventory_categories.csv')])

        print("Importing Inventory Locations")
        import_inventory_locations.Command().handle(csv_file=[open('sample_data/inventory_locations.csv')])

        print("Importing Inventory Manufacturers")
        import_inventory_manufacturers.Command().handle(csv_file=[open('sample_data/inventory_manufacturers.csv')])

        print("Importing Inventory Suppliers")
        import_inventory_suppliers.Command().handle(csv_file=[open('sample_data/inventory_suppliers.csv')])

        print("Importing Inventory Parts")
        import_inventory_parts.Command().handle(csv_file=[open('sample_data/inventory_parts.csv')])

        print("Importing Inventory Orders")
        import_inventory_orders.Command().handle(csv_file=[open('sample_data/inventory_orders.csv')])
