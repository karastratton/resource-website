from django import forms


class BannerIdField(forms.CharField):
    def validate(self, value):
        if len(value) == 9:
            try:
                num_value = int(value)
                if num_value > 900000000 and num_value < 999999999:
                    return True
                else:
                    raise forms.ValidationError('Not a valid Banner ID')
            except:
                raise forms.ValidationError('Not a valid Banner ID')
        else:
            raise forms.ValidationError('Not a valid Banner ID')

    def to_python(self, value):
        return value.replace(' ', '')
      



class UserInfoForm(forms.Form):
    banner_id = BannerIdField(
        label="Banner ID"
        )
